<?php

use yii\db\Migration;
use yii\db\Schema;

class m170510_115009_pdf_table extends Migration
{
    public function up()
    {
        $this->createTable('pdf_file', [
            'id' => Schema::TYPE_PK,
            'pdf_link' => Schema::TYPE_STRING,
        ]);        

        $this->createTable('image_file', [
            'id' => Schema::TYPE_PK,
            'pdf_file_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'image_link' => Schema::TYPE_STRING.' NOT NULL',
        ]);

        $this->createIndex('FK_pdf_file_id', 'image_file', 'pdf_file_id');
        $this->addForeignKey('FK_pdf_file_id', 'image_file', 'pdf_file_id', 'pdf_file', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('image_file');
        $this->dropTable('pdf_file');
    }

}
