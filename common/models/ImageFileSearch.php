<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ImageFile;

/**
 * ImageFileSearch represents the model behind the search form about `common\models\ImageFile`.
 */
class ImageFileSearch extends ImageFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pdf_file_id'], 'integer'],
            [['image_link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImageFile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pdf_file_id' => $this->pdf_file_id,
        ]);

        $query->andFilterWhere(['like', 'image_link', $this->image_link]);

        return $dataProvider;
    }
}
