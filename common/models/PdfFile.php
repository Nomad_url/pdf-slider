<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pdf_file".
 *
 * @property integer $id
 * @property string $pdf_link
 *
 * @property ImageFile[] $imageFiles
 */
class PdfFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pdf_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pdf_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pdf_link' => 'Pdf Link',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImageFiles()
    {
        return $this->hasMany(ImageFile::className(), ['pdf_file_id' => 'id']);
    }

    /*Перед удалением pdf-файла, удаляем все картинки полученные из этого файла*/
    public function beforeDelete() 
     {
        if (parent::beforeDelete()) 
        {
            $images = ImageFile::find()
                ->where('pdf_file_id = :pdf_file_id', [':pdf_file_id' => $this->id])
                ->all();
            if(!empty($images)){
                foreach($images as $img){
                    $img->delete();
                }
            }
            return true;
        } else {
            return false;
        }
    }
}