<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "image_file".
 *
 * @property integer $id
 * @property integer $pdf_file_id
 * @property string $image_link
 *
 * @property PdfFile $pdfFile
 */
class ImageFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pdf_file_id', 'image_link'], 'required'],
            [['pdf_file_id'], 'integer'],
            [['image_link'], 'string', 'max' => 255],
            [['pdf_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => PdfFile::className(), 'targetAttribute' => ['pdf_file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pdf_file_id' => 'Pdf File ID',
            'image_link' => 'Image Link',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPdfFile()
    {
        return $this->hasOne(PdfFile::className(), ['id' => 'pdf_file_id']);
    }
}
