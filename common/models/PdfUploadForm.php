<?php

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class PdfUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;
    public $fileName;

    public function rules()
    {
        return [
            ['file', 'required'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'maxSize' => 50*1024*1024, 'minSize' => 5*1024],
        ];
    }

    public function attributeLabels() {
        return [
            'file' => 'Загружаемый файл',
        ];
    }

    public function upload($pdf_file_id)
    {
        if($this->validate()) {
            $this->file->saveAs(Yii::getAlias('@uploads') ."/". $pdf_file_id ."/". $this->fileName);
            return true;
        }else{
            return false;
        }
    }
}

?>