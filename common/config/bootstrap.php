<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('@template', '@frontend/template');
Yii::setAlias('@uploads', '@frontend/uploads');
Yii::setAlias('@filename', 'presentation.pdf');
Yii::setAlias('@image_name', 'converted_');
Yii::setAlias('@archive_name', 'slider.zip');
