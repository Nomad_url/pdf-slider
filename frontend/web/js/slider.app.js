angular.module('sliderApp', []);
angular.module('sliderApp').controller('ApiController', function($scope, $http, $location) {

    var id = window.location.search.split('=')[1];
    $scope.slides;
    $scope.link;

    $scope.getSlides = function(id) {
        $http.post('get-slides', {
            data: {
                id: id,
                _csrf: yii.getCsrfToken(),
            },
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).success(function(data) {
            $scope.slides = data;
        }).error(function(error) {
        }).finally(function() {
        });
    };

    $scope.getSlides(id);

    $scope.getArchiveSlow = function() {
        $('#progress-bar').show();
        $('#progress-bar .progress-bar').css('width', '50%');
        $http.post('get-archive-slow', {
            data: {
                id: id,
                _csrf: yii.getCsrfToken(),
            },
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).success(function(data) {
            $scope.link = data;
            window.open(data);
        }).error(function(error) {
        }).finally(function() {
            $('#progress-bar .progress-bar').css('width', '100%');
            $('#progress-bar').hide();
        });
    };    

    $scope.getArchiveFast = function() {
        $('#progress-bar').show();
        $('#progress-bar .progress-bar').css('width', '50%');
        $http.post('get-archive-fast', {
            data: {
                id: id,
                _csrf: yii.getCsrfToken(),
            },
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).success(function(data) {
            $scope.link = data;
            window.open(data);
        }).error(function(error) {
        }).finally(function() {
            $('#progress-bar .progress-bar').css('width', '100%');
            $('#progress-bar').hide();
        });
    };
    console.log(id);
});