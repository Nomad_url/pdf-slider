<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\db\Exception;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Url;

use common\models\LoginForm;
use common\models\PdfUploadForm; 
use common\models\PdfFile; 
use common\models\ImageFile; 

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

use tpmanc\imagick\Imagick;

/**
 * Site controller
 */
class SliderController extends Controller
{
    public function beforeAction($action) 
    {
        if ($action->id == 'get-slides' || $action->id == 'get-archive-slow' || $action->id == 'get-archive-fast') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    } 

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $pdf_file = new PdfUploadForm();
        return $this->render('upload-form', ['pdf_file' => $pdf_file]);
    }

    public function actionUpload()
    {
        /*Записываем в переменную имя загружаемого pdf-файла*/
        $filename = Yii::getAlias('@filename');
        /*Создаем объект для загрузки файла*/
        $pdf_file = new PdfUploadForm();
        /*Создаем модель класса PdfFile для сохранения в базу*/
        $model = new PdfFile();
        /*Сохраняем чтобы получить уникальный id, для создания директории*/
        if($model->save()){
            try {
                /*Создаем директорию с уникальным именем (id-модели PdfFile) для хранения файлов презентации*/
                mkdir(Yii::getAlias('@uploads') . "/" . $model->id);
            } catch (Exception $e) {
                Yii::$app->getSession()->setFlash('error', $e->getMessage());
                return $this->redirect('upload-pdf');
            }
        }
        /*Связываем с переменную с формой*/
        $pdf_file->file = UploadedFile::getInstance($pdf_file,'file');
        /*Присваиваем название файлу*/
        $pdf_file->fileName = $filename;
        /*Загружаем файл с передачей параметра id-модели для сохранения в нужную директорию*/
        if($pdf_file->upload($model->id)) {
            /*Добавляем к модели ссылку на файл и сохраняем  в базу*/
            $model->pdf_link = Url::home(true) . "uploads" . "/" . $model->id . "/" . $filename;
            $model->save();
            Yii::$app->getSession()->setFlash('info', 'Файл загружен на сервер.');
            /*Вызываем функцию конвертации, передав в качестве параметров название файла и id-модели для составлению пути до pdf-файла*/
            $this->convert($model->id);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке файла.');
            /*Удаляем запись из бд и директорию, если не удалось загузить файл*/
            try {
                $model->delete();
                rmdir(Yii::getAlias('@uploads') . "/" . $model->id);
            }catch (Exception $e) {
                Yii::$app->getSession()->setFlash('error', $e->getMessage());
            }
            return $this->redirect('index');
        }
    }

    /*Функция конвертирования, принимает на вход название файла и id*/
    protected function convert($pdf_file_id)
    {   
        try {
            /*Создаем директорию для хранения изображений*/
            mkdir(Yii::getAlias('@uploads') . "/" . $pdf_file_id . "/images");
        } catch (Exception $e) {
            Yii::$app->getSession()->setFlash('error', $e->getMessage());
            return $this->redirect('upload-pdf');
        }
        /*Записываем в переменную абсолютный путь до конечной директории*/
        $image_path = Yii::getAlias('@uploads') . "/" . $pdf_file_id . "/images/";
        /*Записываем в переменную абсолютный путь до pdf-файла*/
        $pdf_file_path = Yii::getAlias('@uploads') . "/" . $pdf_file_id . "/" . Yii::getAlias('@filename');
        /*Переменная хранящая количество страниц в pdf-файле*/
        $page_counter = 0;
        /*Проверяем существует ли pdf-файл*/
        if(file_exists($pdf_file_path)){
            /*Получаем количество страниц в pdf-файле*/
            $pdf_file = fopen($pdf_file_path, "r");
            while(!feof($pdf_file)) {
                $line = fgets($pdf_file,255);
                if (preg_match('/\/Count [0-9]+/', $line, $matches)){
                    preg_match('/[0-9]+/',$matches[0], $matches2);
                    if ($page_counter<$matches2[0]) {
                        $page_counter=$matches2[0];
                    } 
                } 
            }
            fclose($pdf_file);

            /*Проверяем на количество страниц, если больше 20 - выход*/
            if($page_counter < 21) {
                /*Перебираем все страницы и сохраняем их в формате jpg*/
                for($i = 0; $i < $page_counter; $i++){
                    $img = Imagick::open($pdf_file_path . "[". $i. "]");
                    if($img){
                        /*Переменная, хранящая название сохраняемого файла*/
                        $image_name = Yii::getAlias('@image_name') . $i . ".jpg";
                        /*Сохраняем используя абсолютный путь*/
                        $img->saveTo($image_path . $image_name);
                        /*Сохраняем модель с относительным путем до файла изображения*/
                        (new ImageFile(['pdf_file_id'=>$pdf_file_id, 'image_link' => Url::home(true) . "uploads" . "/" . $pdf_file_id . "/images/" . $image_name]))->save();
                    }
                }
                Yii::$app->getSession()->setFlash('success', 'Конвертация завершена');
                /*редирект на страницу со слайдами*/
                return $this->redirect(['slider/show-slider', 'id' => $pdf_file_id]);
            }else{
                /*Если число страниц больше 20 - удаляем всю директорию и выводим флэш*/
                $this->removeDirectory(Yii::getAlias('@uploads') . "/" . $pdf_file_id);
                if(($model = PdfFile::findOne($pdf_file_id)) !== null) {
                        $model->delete();
                }
                Yii::$app->getSession()->setFlash('error', "Превышен лимит числа страниц в документе. Максимум - 20 страниц, в загруженном файле - " . $page_counter);
                Yii::$app->getSession()->setFlash('warning', "Файл удален");
                return $this->redirect('index');
            }
        } else {
            Yii::$app->getSession()->setFlash('error', "Файл презентации не найден в " . $pdf_file_path);
            return $this->redirect('index');
        }
    }

    /*Функция возвращает страницу со слайдами, принимая параметром id pdf-файла*/
    public function actionShowSlider($id) 
    {
        return $this->render('slider', ['id' => $id]);
    }

    /*Функция возращает в json-формате список слайдов по переданному id pdf-файла*/
    public function actionGetSlides()
    {
        $params = json_decode(file_get_contents('php://input'));
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($params->data->id){
            $slides = ImageFile::find()->select('image_link')->where('pdf_file_id = :id', [':id' => $params->data->id])->asArray()->all();
            if(!empty($slides)){
                return $slides;
            }else{
                return "Слайдов не найдено";
            }
        } else {
            return "Не передан параметр id";
        }
    }

    /*Функция рекурсивного удаления директорий и всех вложенных в нее поддиректорий и файлов*/
    protected function removeDirectory($path) {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }
    
    /*Функция формирует архив полностью с нуля*/
    /*public function actionSlowArchive(int $id)
    {
        //$start = microtime(true);
        //Записываем в переменную путь до папки хранения презентации
        $slider_path = Yii::getAlias('@uploads') . '/' . $id;
        //Получаем количество изображений
        $slide_number = ImageFile::find()->where('pdf_file_id = :id', [':id' => $id])->count();
        //Если директория существует и количество слайдов в базе не равно 0, то добавляем в архив
        if(file_exists($slider_path) && $slide_number){
            $this->addToZip(Yii::getAlias('@template'), $slider_path);
            $this->addToZip($slider_path, $slider_path);
            $this->addJSParam($id, $slide_number);
            //return microtime(true) - $start;
            return $this->redirect(Url::home(true) . 'uploads' . '/' . $id . '/' . Yii::getAlias('@archive_name'));
        }else{
            throw new NotFoundHttpException('The requested slider does not exist.');
        }
    }*/

    /*Функция формирования архива на основе подготовленного zip-архива с папкой assets и файлом index.html*/
    /*public function actionArchive(int $id)
    {
        //$start = microtime(true);
        $path = Yii::getAlias('@uploads') . '/' . $id;
        $slide_number = ImageFile::find()->where('pdf_file_id = :id', [':id' => $id])->count();
        if(file_exists($path) && $slide_number){
            try {
                copy(Yii::getAlias('@template') . '/' . Yii::getAlias('@archive_name'), $path . '/' . Yii::getAlias('@archive_name'));
            } catch (Exception $e) {
                return $e->getMessage();
            }
            $dir_files = glob($path.'/images/*');
            foreach ($dir_files as $iterator) {
                if(is_file($iterator)) {
                    $zip = new \ZipArchive();
                    $zip->open($path . '/' . Yii::getAlias('@archive_name'), \ZipArchive::CREATE);
                    $zip->addFile($iterator, str_replace($path . '/', '', $iterator));
                    $zip->close();
                }
            }
            $this->addJSParam($id, $slide_number);
            return $this->redirect(Url::home(true) . "uploads" . "/" . $id . "/" . Yii::getAlias('@archive_name'));
            //return microtime(true) - $start;
        } else{
            throw new NotFoundHttpException('The requested slider does not exist.');
        }
    }*/

    /*Функция рекурсивного добавления файлов в архив*/
    protected function addToZip($source, $dist, $source_base=null)
    {
        /*Переменная хранит абсолютную часть пути до файла, являясь доменом 1-го уровня*/
        $source_base = $source_base ?? $source;
        /*Получаем список всех файлов и поддиректорий*/
        $dir_files = glob($source . '/*[^.(zip|pdf)]');
        foreach ($dir_files as $iterator) {
            /*Если файл - добавляем в архив*/
            if(is_file($iterator)) {
                $zip = new \ZipArchive();
                /*Открываем архив по заданному пути, если нет - создаем*/
                $zip->open($dist . "/" . Yii::getAlias('@archive_name'), \ZipArchive::CREATE);
                /*Отсекаем абсолютную часть пути до файла, оставляя часть дочернюю относительно корня добавляемой в архив папки*/
                $zip->addFile($iterator, str_replace($source_base . '/', '', $iterator));
                $zip->close();
            }else{
                /*Если папка - рекурсивный вызов*/
                $this->addToZip($iterator, $dist, $source_base);
            }
        }
        return;
    }    

    /*Функция добавляет параметр количество слайдов в js-файл слайдера*/
    protected function addJSParam($id, $slide_number)
    {
        $zip = new \ZipArchive;
        $archive = Yii::getAlias('@uploads') . '/'. $id . '/' . Yii::getAlias('@archive_name');
        if($zip->open($archive) === TRUE) {
            /*Получаем строку из js файла шаблона и подставляем параметр - количество слайдов*/
            $js_code = sprintf(file_get_contents(Yii::getAlias('@template') . '/assets/js/slider.app.js'), $slide_number);
            /*Добавляем сформированный файл к архиву*/
            $zip->addFromString('assets/js/slider.app.js', $js_code);
            return $zip->close();
        }else{
            throw new NotFoundHttpException('The requested archive does not exist.');
        }
    }

    /*Функция получает аякс запросом параметр id-pdf файла и формирует архив с нуля, возвращая ссылку на него*/
    public function actionGetArchiveSlow()
    {
        $params = json_decode(file_get_contents('php://input'));
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //Если параметр указан-продолжаем
        if($params->data->id) {
            $id = $params->data->id;
            /*Записываем в переменную путь до папки хранения презентации*/
            $slider_path = Yii::getAlias('@uploads') . '/' . $id;
            /*Получаем количество изображений*/
            $slide_number = ImageFile::find()->where('pdf_file_id = :id', [':id' => $id])->count();
            /*Если директория существует и количество слайдов в базе не равно 0, то добавляем в архив*/
            if(file_exists($slider_path) && $slide_number){
                //Добавляем в архив неизменные части презентации - assets и index.html
                $this->addToZip(Yii::getAlias('@template'), $slider_path);
                //Добавляем изображения в архив
                $this->addToZip($slider_path, $slider_path);
                //Добавляем параметр в js файл для слайдера
                $this->addJSParam($id, $slide_number);
                return Url::home(true) . 'uploads' . '/' . $id . '/' . Yii::getAlias('@archive_name');
            }else{
                throw new NotFoundHttpException('The requested slider does not exist.');
            }
        }else{
            return 'No params given';
        }
    }

    /*Функция формирования архива на основе подготовленного zip-архива с папкой assets и файлом index.html*/
    public function actionGetArchiveFast()
    {
        $params = json_decode(file_get_contents('php://input'));
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //Если параметр указан-продолжаем
        if($params->data->id) {
            $id = $params->data->id;
            /*Записываем путь до конечной директории*/
            $path = Yii::getAlias('@uploads') . '/' . $id;
            /*Получаем количество изображений*/
            $slide_number = ImageFile::find()->where('pdf_file_id = :id', [':id' => $id])->count();
            if(file_exists($path) && $slide_number){
                try {
                    /*Копируем подготовленный архив-образец, содержащий папку assets и файл index.html*/
                    copy(Yii::getAlias('@template') . '/' . Yii::getAlias('@archive_name'), $path . '/' . Yii::getAlias('@archive_name'));
                } catch (Exception $e) {
                    return $e->getMessage();
                }
                /*Путь до изображений*/
                $dir_files = glob($path.'/images/*');
                /*Добавляем в архив изображения*/
                foreach ($dir_files as $iterator) {
                    if(is_file($iterator)) {
                        $zip = new \ZipArchive();
                        $zip->open($path . '/' . Yii::getAlias('@archive_name'), \ZipArchive::CREATE);
                        $zip->addFile($iterator, str_replace($path . '/', '', $iterator));
                        $zip->close();
                    }
                }
                /*Подставляем параметр количество слайдов в js-файл*/
                $this->addJSParam($id, $slide_number);
                return Url::home(true) . "uploads" . "/" . $id . "/" . Yii::getAlias('@archive_name');
            } else{
                throw new NotFoundHttpException('The requested slider does not exist.');
            }
        }else{
            return 'No params given';
        }
    }
}
