<?php

namespace frontend\controllers\rest;

use Yii;
use common\models\ImageFile;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class ImageController extends Controller
{
    public function actionGetImages($id)
    {
        return ImageFile::find()->select('image_link')->where('pdf_file_id = :id',[':id' => $id])->all();
    }
}
