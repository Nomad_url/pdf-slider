<?php

use yii\helpers\Html;

$this->title = 'Slider';

?>
<div class="slider" ng-controller="ApiController">
	<div class="row">
		<div class="col-md-12">
			<div id="carousel-generic" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
				    <div ng-repeat="slide in slides" class="item" ng-class="{true:'active'}[$index==0]">
				    	<div class="text-center">
				    		<img ng-src="{{slide.image_link}}"/>
				    	</div>
				    </div>
				</div>
				<a class="left carousel-control" href="#carousel-generic" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-generic" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
	</div>
	<div class="row" id="archive-download">
		<div class="col-md-12 text-right">
			<p>
        		<?= Html::button('Скачать', ['class' => 'btn btn-success', 'ng-click' => 'getArchiveSlow()']) ?>
        		<?= Html::button('Скачать быстро', ['class' => 'btn btn-primary', 'ng-click' => 'getArchiveFast()']) ?>
        	</p>
		</div>
	</div>
	<div class="row" id="download-link">
		<div class="col-md-12 text-center">
			<a ng-if="link" href="{{link}}">Cсылка для скачивания файла будет активна в течении 30 минут</a>
		</div>
		
	</div>

</div>