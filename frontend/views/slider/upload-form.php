<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Загрузка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider">

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
		        <div class="panel-heading text-center">
		        	<h3>Загрузить файл</h3>
		        </div>
		        <div class="panel-body">

					<?php $form = ActiveForm::begin([
						'action' => ['slider/upload'], 
						'options' => ['enctype' => 'multipart/form-data'],
					]); ?>

					<?=	$form->field($pdf_file, 'file')->fileInput()->label('Файл') ?>

					<div class="form-group">
							
						<?= Html::submitButton('Конвертировать', ['class' => 'btn btn-success']) ?>

					</div>

					<?php ActiveForm::end(); ?>

		  		</div>
		    </div>	
		</div>
	</div>

</div>