<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PdfFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pdf Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pdf-file-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'layout' => "{items}",
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'pdf_link',
            [
            'attribute' => 'pdf_link',
            'format' => 'html',
            'value' => function($model) {
                    return Html::a($model->pdf_link, $model->pdf_link, ['target' => '_blank']);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
                'buttons' => [
                    'delete' => function ($url,$model) {
                        return Html::a('Удалить', ['delete', 'id'=>$model->id], ['class' => 'btn btn-danger']);
                    },
                    'view' => function($url, $model) {
                        return Html::a('Презентация', ['slider/show-slider', 'id' => $model->id], ['class' => 'btn btn-primary']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
