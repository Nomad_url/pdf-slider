<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PdfFile */

$this->title = 'Create Pdf File';
$this->params['breadcrumbs'][] = ['label' => 'Pdf Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pdf-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
