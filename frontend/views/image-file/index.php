<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ImageFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Image Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-file-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'layout' => "{items}",
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'pdf_file_id',
            //'image_link',
            [
            'attribute' => 'image_link',
            'format' => 'html',
            'value' => function($model) {
                    return Html::a($model->image_link, $model->image_link, ['target'=>'_blank']);
                }
            ],
            [
            'label' => 'Image',
            'format' => 'raw',
            'value' => function($data){
                return Html::img($data->image_link,[
                    'style' => 'width:150px;'
                ]);
            },
        ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete}'
            ],
        ],
    ]); ?>
</div>
