<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ImageFile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-file-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pdf_file_id')->textInput() ?>

    <?= $form->field($model, 'image_link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
